class PIDReg:
    def __init__(self, KP = 0, KD = 0, KI = 0, DAMP = 1):
        self.KP = KP
        self.KD = KD
        self.KI = KI
        self.DAMP = DAMP
        self.integrative = 0
        self.lastError = 0
    def __copy__(self):
        return PIDReg(self.KP, self.KD, self.KI, self.DAMP)
    def regulate(self, error):
        self.derivative = error-self.lastError
        self.integrative = self.integrative*self.DAMP + error
        correction = error*self.KP + self.derivative*self.KD + self.integrative*self.KI
        self.lastError = error
        return correction
    def __repr__(self):
        return "KP: %s KD: %s KI: %s DAMP: %s" % (self.KP, self.KD, self.KI, self.DAMP)
    def reset(self):
        self.integrative = 0
        self.lastError = 0
