#!/usr/bin/env pybricks-micropython
# exec(open("missions.py").read())
# "delete this" comment
from magic_robot import Robot 
from PIDReg import PIDReg
from time import time
from pybricks.tools import wait, StopWatch
from pybricks import ev3brick as brick
import motors
import sensors

def color_calibration(robot):
    sensors.light_calibrate()

def mission_1(robot):
    angle = 0
    sensors.gyro.reset_angle(angle)
    angle = 10
    robot.spin(angle, KI=0.02)
    robot.distance_straight_walk(angle, 50, 10)
    angle = 0  
    robot.spin(angle, KI=0.02)
    robot.distance_straight_walk(angle, 100, 80) 
    angle = 45
    robot.spin(angle)
    robot.distance_straight_walk(angle, 50, 30)
    angle = 0
    robot.spin(0)
    robot.timed_straight_walk(angle, 100, 1)
    robot.timed_straight_walk(angle, 50, 1.5)
    robot.distance_straight_walk(angle, -50, -30)
    angle = 25
    robot.spin(angle, KI=0.02)
    motors.stall_the_motor('medium_left', -50)
    robot.timed_straight_walk(angle, 40, 3)
    motors.stall_the_motor('medium_left', 50)
    robot.distance_straight_walk(angle, -100, -45)
    angle = 0
    robot.spin(angle)
    robot.distance_straight_walk(angle, 70, 53)
    robot.timed_straight_walk(angle, -100, 10)


def mission_2(robot):
    angle = 0
    sensors.gyro.reset_angle(angle)
    angle = 4
    motors.stall_the_motor('medium_right', -100, 70)
    robot.distance_straight_walk(angle, 90, 46)
    robot.distance_straight_walk(angle, -100, -32)
    angle = -90
    robot.spin(angle)
    robot.distance_straight_walk(angle, 60, 25)
    angle = 3
    robot.spin(angle)
    robot.distance_straight_walk(angle, 100, 36) 
    robot.timed_straight_walk(angle, 30, 2)
    motors.stall_the_motor('medium_right', 100, 70)
    motors.stall_the_motor('medium_right', -100, 70)
    angle = 0
    robot.distance_straight_walk(angle, -90, -30)
    robot.timed_straight_walk(angle, -90, 3)
    robot.distance_straight_walk(angle, 30, 5)
    angle = 45
    robot.timed_straight_walk(angle, 100, 3)
    angle = 25
    robot.timed_straight_walk(angle, 100, 3.2)

def mission_3(robot):
    angle = 0
    sensors.gyro.reset_angle(angle)
    motors.stall_the_motor('medium_right', -50)
    angle = 45
    robot.spin(angle)
    robot.distance_straight_walk(angle, 80, 58)
    motors.stall_the_motor('medium_right', 50)
    robot.distance_straight_walk(angle, 40, 15)
    motors.medium_left.run_angle(10000, 270)
    motors.stall_the_motor('medium_right', -50)
    motors.medium_left.run_angle(10000, 270)
    robot.distance_straight_walk(angle, -20, -10)
    angle = 90
    robot.spin(angle)
    robot.distance_straight_walk(angle, 80, 27)
    angle = 50
    robot.spin(angle)
    robot.timed_straight_walk(angle, 30, 0.7)
    motors.stall_the_motor('medium_right', 50)
    angle = 60
    robot.spin(angle)
    robot.distance_straight_walk(angle, 25, 6)
    angle = 50
    robot.spin(angle)
    motors.stall_the_motor('medium_right', -50)
    robot.distance_straight_walk(angle, -25, -6)
    angle = 90
    robot.spin(angle)
    robot.distance_straight_walk(angle, -100, -70)
    angle = 0
    robot.spin(angle)
    robot.timed_straight_walk(angle, -100, 4)
    motors.off_motors()

def mission_4(robot):
    angle = 0
    sensors.gyro.reset_angle(angle)
    robot.distance_straight_walk(angle, 80, 78)
    robot.distance_straight_walk(angle, -70, -60)
    angle = -15
    robot.spin(angle)
    robot.distance_straight_walk(angle, 100, 105)
    angle += 90
    robot.spin(angle)
    robot.distance_straight_walk(angle, 70, 27)
    angle += 30
    robot.spin(angle)
    robot.distance_straight_walk(angle, 100, 90)

def do_mission(robot, mission):
    try:
        wait(400)
        if mission == -1:
            sensors.set_gyro_sensor(Port.S2)
            motors.off_motors()
        elif mission == 0:
            color_calibration(robot)
            motors.off_motors()
        elif mission == 1:
            mission_1(robot)
            motors.off_motors()
        elif mission == 2:
            mission_2(robot)
            motors.off_motors()
        elif mission == 3:
            # mission_3(robot)
            motors.off_motors()
        elif mission == 4:
            mission_4(robot)
            motors.off_motors()
        else:
            print("UNKNOWN MISSION!")
    except KeyboardInterrupt:
        motors.off_motors()
        wait(1000)
    return 0

robby = Robot()
try:
    if __name__ == "__main__":
        main_stop_watch = StopWatch()
        while True:
            pressed_buttons = brick.buttons()
            if 256 in pressed_buttons:
                do_mission(robby, 1)
            elif 32 in pressed_buttons:
                do_mission(robby, 2)
            elif 4 in pressed_buttons:
                do_mission(robby, 3)
            elif 16 in pressed_buttons:
                do_mission(robby, 4)
finally:
    motors.off_motors()
