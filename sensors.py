from pybricks import ev3brick as brick
from pybricks.ev3devices import (ColorSensor, GyroSensor)
from pybricks.parameters import (Port, Direction, Color)
from pybricks.tools import print, wait

black = 4
white = 61.5
color_sensitivity = 10
distance_sensitivity = 0
left_sensor = ColorSensor(Port.S3)
center_sensor = ColorSensor(Port.S1)
right_sensor = ColorSensor(Port.S4)
gyro = GyroSensor(Port.S2)
gyro._calibrate()


def set_left_sensor(left_sensor_port):
    global left_sensor
    left_sensor = ColorSensor(left_sensor_port)


def set_center_sensor(center_sensor_port):
    global center_sensor
    center_sensor = ColorSensor(center_sensor_port)


def set_right_sensor(right_sensor_port):
    global right_sensor
    right_sensor = ColorSensor(right_sensor_port)


def set_gyro_sensor(gyro_sensor_port):
    global gyro
    gyro = GyroSensor(set_gyro_sensor)
    gyro._calibrate()


# function for reading the gyro sensors
# is made such that the sensor will not be reseteted when we read speed or position
def my_gyro(mode = 'angle'):
    angle, rate = gyro.angle(), gyro.speed()
    if mode == 'angle':
        return angle
    elif mode == 'rate':
        return rate
    elif mode == 'angle_and_rate':
        return angle, rate
    else:
        print("ERROR: UNKNOWN MODE 'angle' 'rate' OR 'angle_and_rate'")


# function for calibrate the light sensors
def light_calibrate():
    global black
    global white
    #TODO: logging
    print('Calibration, ready!')
    print('White')
    input()
    input()
    sleep(0.5)
    max1 = left_sensor.reflection() 
    max2 = right_sensor.reflection()
    print('Black, ready!')
    input()
    # input()
    min1 = left_sensor.reflection()
    min2 = right_sensor.reflection()
    white = (max1+max2)/2
    black = (min1+min2)/2
    print('White ', white)
    print('Black ', black)
    input()
    input()

# function for reading the light sensor
# can use red light, white light or can return RGB color
def light(sensor, mode='red'):
    if sensor == 'left':
        sensor = left_sensor
    elif sensor == 'center':
        sensor = center_sensor
    elif sensor == 'right':
        sensor = right_sensor
    else:
        if isinstance(sensor, ColorSensor):
            pass
        else:
            print("COLOR SENSOR ERROR: 'left' 'right' OR ColorSensor OBJECT ")

    if mode == 'red':
        return (sensor.reflection() - black) * (100/(white-black))
    elif mode == 'white':
        R, G, B = sensor.rgb()
        return (R+G+B)/7.65
    elif mode == 'rgb':
        return sensor.rgb()
    else:
        print(" COLOR MODE ERROR 'red' OR 'rgb' OR 'white' ")
        return None
    # print the color in RGB
# try to paint the text in the seen color
def print_color(sensor):
    if sensor == 'left':
        sensor = left_sensor
    elif sensor == 'right':
        sensor = right_sensor
    else:
        print(" ERROR, sensor should be either 'left' or 'right' ")
        return 0
    R, G, B = light(sensor, mode='rgb')
    print('\033[38;2;' + str(R) + ';' + str(G) + ';' + str(100) + 'm', R, G, B, '\033[38;2;0m')
