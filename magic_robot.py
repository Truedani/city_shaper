#!/usr/bin/env pybricks-micropython

from pybricks import ev3brick as brick
from pybricks.ev3devices import (Motor, ColorSensor, GyroSensor)
from pybricks.parameters import (Port, Stop, Direction, Button, Color)
from pybricks.tools import print, wait, StopWatch
from math import pi, floor
from PIDReg import PIDReg

import motors
import sensors

def exit_handler():
    motors.off_motors()

# main class, defines the attributes and methods of the Robot
class Robot:
    # init function, called once when a new object is made
    def __init__(self, polarity=1,  diameter = 4.32):
        self.stop_watch = StopWatch()
        self.color_sensitivity = 10
        self.distance_sensitivity = 0
        self.diameter = diameter
        self.walk_PID_angle = PIDReg(KP = 4)
        self.walk_PID_velocity = PIDReg(KP = 0.8)
        self.walk_PID_distance = PIDReg(KP = 0.3, KI=0.02, DAMP=1)
        self.SPEED_BOOST = 1.0
    
    def __del__(self):
        motors.off_motors()
    # speed correction function
    def walk_open_loop(self, set_point, base_speed):
        # self.print_color('left')
        # if self.left_motor.is_overloaded or self.right_motor.is_overloaded:
        left_speed, right_speed = motors.read_speeds()
        # if base_speed > 0:
        #     base_speed = min(left_speed, right_speed) - 5
        # else:
        #    base_speed = max(left_speed, right_speed) - 5
        angle, velocity = sensors.my_gyro('angle_and_rate') 
        angle_error = set_point - angle
        velocity_error = 0 - velocity
        correction = self.walk_PID_angle.regulate(angle_error) + self.walk_PID_velocity.regulate(velocity_error)
        left_speed = base_speed - correction
        right_speed = base_speed + correction
        motors.my_motors(left_speed, right_speed)

    # power correction function
    def walk_open_loop2(self, set_point, base_speed):
        angle, velocity = sensors.my_gyro('angle_and_rate') 
        angle_error = set_point - angle
        velocity_error = 0 - velocity
        correction = self.walk_PID_angle.regulate(angle_error) + self.walk_PID_velocity.regulate(velocity_error)
        left_speed = base_speed - correction
        right_speed = base_speed + correction
        motors.power_motors(left_speed, right_speed)

    # function for accelerating or deccelerating
    def change_speeds(self, new_left_speed, new_right_speed, acceleration_time = 20):
        # acceleration time in microseconds
        left_speed, right_speed = motors.read_speeds()
        left_change = new_left_speed - left_speed
        right_change = new_right_speed - right_speed
        numitor = max(abs(left_change), abs(right_change))
        left_rate = left_change / numitor
        right_rate = right_change / numitor
        acceleration_time *= 1000000
        while numitor >= 0:
            if(brick.buttons() != []):
                raise KeyboardInterrupt
            numitor -= 1
            left_speed += left_rate
            right_speed += right_rate
            motors.my_motors(left_speed, right_speed)
            wait(acceleration_time)
        else:
            motors.my_motors(new_left_speed, new_right_speed)
        left_speed, right_speed = self.read_speeds()

    # stop timer function, return True when target time is reached
    def timed_stop(self, target_time, init = False):
        if(init):
            self.stop_watch.reset()
        else:
            if(self.stop_watch.time() > target_time*1000):
                return True
        return False

    # stop function for distance straight walk
    # control the last bit of movement and stops when target reached
    def distance_stop(self, target_distance, set_point, init = False):
        if init:
            self.sign = lambda x: (1, -1)[x < 0]
            self.direction = self.sign(target_distance)
            self.degrees = target_distance*360 / ( self.diameter*pi) + (motors.large_left_motor.angle() + motors.large_left_motor.angle() )/2
            self.go_easy = self.degrees - self.direction*5*360/(self.diameter*pi)   
        else:
            if self.direction*( motors.large_left_motor.angle() + motors.large_left_motor.angle() )/2 > self.direction*self.go_easy:
                speed_left, speed_right = motors.read_speeds()
                base_speed = (speed_left + speed_right) / 2
                speed = base_speed
                while((speed > self.distance_sensitivity or speed < -self.distance_sensitivity)):
                    if(brick.buttons() != []):
                        raise KeyboardInterrupt
                    error = self.degrees - (motors.large_left_motor.angle()
+ motors.large_left_motor.angle())/2
                    correction = self.walk_PID_distance.regulate(error) 
                    speed = self.sign(correction)*floor(min(abs(correction), abs(base_speed)))
                    self.walk_open_loop(set_point, base_speed = speed)
                return True
        return False

    # stop function for walking until a treeshold value is reached
    # when the sensor value is close to the treeshold value, return True
    def color_stop(self, treeshold, sensor, mode='red', init = False):
        sensor_value = sensors.light(sensor, mode=mode)
        # print('Sensor: ', sensor_value)
        if init:
            pass
        else:
            if sensor_value < treeshold + self.color_sensitivity and sensor_value > treeshold - self.color_sensitivity:
                return True
        return False

    # universal straight_walk function
    # needs a stop function to stop the movement
    def straight_walk(self, set_point, base_speed, stop_function, *args, brake=True, mode='power'):
        stop_function(*args, init = True)
        base_speed *= self.SPEED_BOOST
        if mode == 'power':
            loop_function = self.walk_open_loop2
        elif mode == 'speed':
            loop_function = self.walk_open_loop
        else:
            print("UNKOWN MODE, MUST BE: 'power' OR 'speed'")
        while(stop_function(*args) == False):
            loop_function(set_point = set_point, base_speed = base_speed)

        if brake:
            motors.large_left_motor.stop(brake)
            motors.large_right_motor.stop(brake)
        # self# motors.off()
        # input()
        
    # redefinition of straight_walk function
    # for distance, timed and color stop functionpolaritys
    # made such these will be easier to use
    def distance_straight_walk(self, angle, speed, distance, brake=True, mode='power'):
        self.straight_walk(angle, speed, self.distance_stop, distance, angle, brake=brake, mode=mode)

    def timed_straight_walk(self, angle, speed, delay, brake=True, mode='power'):
        self.straight_walk(angle, speed, self.timed_stop, delay, brake=brake, mode=mode)

    def color_straight_walk(self, angle, speed, treeshold, sensor, brake=True, mode='power'):
        if sensor == 'left':
            sensor = sensors.left_sensor
        elif sensor == 'right':
            sensor = sensors.right_sensor
        else:
            print(" ERROR, sensor should be either 'left' or 'right' ")
            return 0
        self.straight_walk(angle, speed, self.color_stop, treeshold, sensor, brake=brake, mode=mode)

    # function for spinning the robot to a given angle
    def spin(self, target, max_speed = 25, KP=1, KI=0.003, KD=2, DAMP=1):
        # print()
        # print(sensors.my_gyro())
        sensor = sensors.my_gyro()
        error = target - sensor
        speed_left = 0
        speed_right = 0
        PID_spin = PIDReg(KP=KP, KI=KI, KD=KD)
        while (error != 0):
            correction = PID_spin.regulate(error)
            speed = min(correction, max_speed)
            speed_left = -speed
            speed_right = speed
            motors.my_motors(speed_left, speed_right)
            sensor = sensors.my_gyro()
            error = target - sensor
        motors.large_left_motor.stop(True)
        motors.large_right_motor.stop(True)
        # input()
    
    # print the color in RGB
    # try to paint the text in the seen color
    def print_color(self, sensor):
        if sensor == 'left':
            sensor = sensors.left_sensor
        elif sensor == 'right':
            sensor = sensors.right_sensor
        else:
            print(" ERROR, sensor should be either 'left' or 'right' ")
            return 0
        R, G, B = sensors.light(sensor, mode='rgb')
        print('\033[38;2;' + str(R) + ';' + str(G) + ';' + str(100) + 'm', R, G, B, '\033[38;2;0m')

try:
    if __name__ == "__main__":
        main_stop_watch = StopWatch()
        robby = Robot()
        mission_one(robby)
        
        print(main_stop_watch.time())
finally:
        exit_handler()
