#!/usr/bin/env pybricks-micropython

from pybricks import ev3brick as brick
from pybricks.ev3devices import (Motor, ColorSensor, GyroSensor)
from pybricks.parameters import (Port, Stop, Direction, Button, Color)
from pybricks.tools import print, wait, StopWatch
from math import pi, floor
from PIDReg import PIDReg

large_left_motor    = Motor(Port.B, 1) 
large_right_motor   = Motor(Port.C, 1)
medium_left_motor   = Motor(Port.A)
medium_right_motor  = Motor(Port.D)

def set_large_left_motor(large_left_motor_port, polarity = 1):
    global large_left_motor
    large_left_motor = Motor(large_left_motor_port, polarity)

def set_large_right_motor(large_right_motor_port, polarity = 1):
    global large_right_motor
    large_right_motor = Motor(large_right_motor_port, polarity)

def set_medium_left_motor(medium_left_motor_port):
    global medium_left_motor
    medium_left_motor = Motor(medium_left_motor_port)

def set_medium_right_motor(medium_right_motor_port):
    global medium_right_motor
    medium_right_motor = Motor(medium_right_motor_port)

def set_motors(left_motor = Port.B, right_motor = Port.C, medium_left = Port.A, medium_right = Port.D):
    large_left_motor = Motor(left_motor, polarity)
    large_right_motor = Motor(right_motor, polarity)
    medium_left_motor = Motor(medium_left)
    medium_right_motor = Motor(medium_right)

def set_limits(value, max_value):
    sign = lambda x: (1, -1)[x < 0]
    if abs(value) <= max_value:
        return value
    else:
        return sign(value)*max_value

def my_motors(left_speed, right_speed, max_speed = 100):
    if(brick.buttons() != []):
        raise KeyboardInterrupt
    left_speed  = set_limits(left_speed, max_speed)
    right_speed = set_limits(right_speed, max_speed)
    large_left_motor.run(left_speed*7)
    large_right_motor.run(right_speed*7)

def off_motors(brake=False):
    large_left_motor.stop(brake)
    large_right_motor.stop(brake)
    medium_left_motor.stop(brake)
    medium_right_motor.stop(brake)

# read and return the speed
# for Large Motors the returned value is close to percentage
# for Medium Motors is aprox x1.5 percentage value
def read_speeds():
    return large_left_motor.speed()/7, large_right_motor.speed()/7
 
def power_motors(power_left, power_right, max_power = 100, brake=False):
    if(brick.buttons() != []):
        raise KeyboardInterrupt
    if power_left == 0:
        large_left_motor.stop(brake)
    else:
        power_left = set_limits(power_left, max_power)
        large_left_motor.dc(power_left)
    if power_right == 0:
        large_right_motor.stop(brake)
    else:
        power_right = set_limits(power_right, max_power)
        large_right_motor.dc(power_right)
    
def stall_the_motor(motor, speed, power_limit=50, brake=False):
    if motor == 'medium_left':
        motor = medium_left_motor
    elif motor == 'medium_right':
        motor = medium_right_motor
    elif motor == 'left':
        motor = large_left_motor
    elif motor == 'right':
        motor = large_right_motor
    else:
        print("UNKNOWN MOTOR; we have: 'medium_left' 'medium_right' 'left' 'right'")
        return 0
    motor.run_until_stalled(speed*160, brake, power_limit)
