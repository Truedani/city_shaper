#!/usr/bin/env pybricks-micropython

from pybricks import ev3brick as brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import (Port, Stop, Direction, Button, Color,
                                 SoundFile, ImageFile, Align)
from pybricks.tools import print, wait, StopWatch
from pybricks.robotics import DriveBase

from magic_robot import Robot
from missions import do_mission
import motors

brick.sound.beep()

robby = Robot()
try:
    if __name__ == "__main__":
        main_stop_watch = StopWatch()
        print("Program ready")
        while True:
            pressed_buttons = brick.buttons()
            if 256 in pressed_buttons:
                print("Mission one")
                do_mission(robby, 1)
            elif 32 in pressed_buttons:
                print("Mission two")
                do_mission(robby, 2)
            elif 4 in pressed_buttons:
                print("Mission three")
                do_mission(robby, 3)
            elif 16 in pressed_buttons:
                print("Mission four")
                do_mission(robby, 4)
finally:
    motors.off_motors()
